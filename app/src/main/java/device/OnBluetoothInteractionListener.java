package device;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Interface with methods to implement basic reactions to BLE interaction
 */
public interface OnBluetoothInteractionListener {
    void onDisconnect();
    void onConnect();
    void onDiscovered();
    void onRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);
    void onWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status);
    void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic);
}
