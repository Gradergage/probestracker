package device;

import java.util.ArrayList;

/**
 * Created by Gradergage on 01.06.2018.
 */

public interface TrackerAdapter {
    public void getNewHeartRate();
    public void getNewStepCount();
    public boolean connectToDevice();
    public boolean setContiniousHeartrate(boolean enable);
    // public ArrayList<String> getDevicesList();
    public void disconnect();
    public boolean isConnectedToDevice();
    public void setNotificationButton();
    public boolean setNotificationSteps(boolean enable);
    public void setNotificationHeartRate();
    public int getStepsFromByteValue(byte[] data);
    public int getHeartRateFromByteValue(byte[] data);
}
