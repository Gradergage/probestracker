package device;

import java.util.UUID;

public class MiBandServices {


    public static final String BASE_UUID = "0000%s-0000-1000-8000-00805f9b34fb";
    public static final UUID UUID_SERVICE_GENERIC = UUID.fromString(String.format(BASE_UUID, "1800"));
    public static final UUID UUID_MIBAND1_BASE_SERVICE = UUID.fromString(String.format(BASE_UUID, "FEE0"));
    public static final UUID UUID_MIBAND2_BASE_SERVICE = UUID.fromString(String.format(BASE_UUID, "FEE1"));
    public static final UUID UUID_CHARACTERISTIC_DEVICE_NAME = UUID.fromString(String.format(BASE_UUID, "2A00"));
    public static final UUID UUID_TOUCH_BUTTON = UUID.fromString("00000010-0000-3512-2118-0009af100700");
    public static final UUID UUID_CHARACTERISTIC_STEPS = UUID.fromString("00000007-0000-3512-2118-0009af100700");
    public static final UUID UUID_CHAR_CONTROL_POINT = UUID.fromString("0000ff05-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_SERVICE_HEARTBEAT = UUID.fromString(String.format(BASE_UUID, "180D"));
    public static final UUID UUID_HEARTRATE_CONTROL_POINT = UUID.fromString(String.format(BASE_UUID, "2A39"));
    public static final UUID UUID_HEARTRATE_MEASUREMENT = UUID.fromString(String.format(BASE_UUID, "2A37"));
    public static final byte COMMAND_SET__HR_CONTINUOUS = 0x1;
 // public static final byte COMMAND_SET_HR_SLEEP = 0x0;
    public static final byte COMMAND_SET_HR_SLEEP = 0x0;
    public static final byte COMMAND_SET_PERIODIC_HEARTRATE_SCAN = 0x14;
    public static final byte[] BYTE_LAST_HEART_RATE_SCAN = {21, 1, 1};
    public static final byte[] BYTE_NEW_HEART_RATE_SCAN = {21, 2, 1};
    public static final byte[] BYTE_ENABLE_REALTIME_STEPS_NOTIFY = {3, 1};
    public static final byte[] startHeartMeasurementContinuous = new byte[]{0x15, MiBandServices.COMMAND_SET__HR_CONTINUOUS, 1};
    public static final byte[] stopHeartMeasurementContinuous = new byte[]{0x15, MiBandServices.COMMAND_SET__HR_CONTINUOUS, 0};
    public static final UUID UUID_NOTIFICATION_DESCRIPTOR = UUID.fromString(String.format(BASE_UUID, "2902"));
    //public static final UUID UUID_NOTIFICATION_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
}
