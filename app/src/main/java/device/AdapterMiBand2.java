package device;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;

import java.util.ArrayList;

import static java.lang.Thread.sleep;

/**
 * @author Gradergage
 */
public class AdapterMiBand2 implements TrackerAdapter {

    public static final String TAG = "AdapterMiBand2";
    public static final String NAME = "MI";
    final BluetoothAdapter myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    AdapterBLE adapterBLE;
    public boolean notifyStepsEnabled = false;
    public boolean isConnectedToDevice = false;

    public AdapterMiBand2(AdapterBLE adapterBLE) {
        this.adapterBLE = adapterBLE;
    }

    @Override
    public boolean isConnectedToDevice() {
        return isConnectedToDevice;
    }

    /**
     * Connect to bracer using device name and default bluetooth adapter
     */
    public boolean connectToDevice() {

        // Setup Bluetooth:
        if (!adapterBLE.getBluetoothDeviceByName(myBluetoothAdapter, NAME))
            return false;
        // adapterBLE.connect();
        isConnectedToDevice = adapterBLE.connect();
        return isConnectedToDevice;
    }

  /*  public ArrayList<String> getDevicesList() {
        return adapterBLE.getDevicesList(myBluetoothAdapter);
    }*/

    @Override
    public void disconnect() {
        adapterBLE.disconnect();
        isConnectedToDevice = false;
    }

    /**
     * Writing request to start new heartrate measurement
     */
    public boolean setContiniousHeartrate(boolean enable) {
        if (adapterBLE == null || !adapterBLE.isConnected()) {
            Log.d(TAG, " !!! Device not connected");
            return false;
        }
        boolean status;
        if (enable) {
            status = adapterBLE.writeData(MiBandServices.UUID_SERVICE_HEARTBEAT,
                    MiBandServices.UUID_HEARTRATE_CONTROL_POINT,
                    MiBandServices.startHeartMeasurementContinuous);
        } else {
            status = adapterBLE.writeData(MiBandServices.UUID_SERVICE_HEARTBEAT,
                    MiBandServices.UUID_HEARTRATE_CONTROL_POINT,
                    MiBandServices.stopHeartMeasurementContinuous);

        }
        Log.d(TAG, "setContiniousHeartrate: enable " + enable + " status " + status);
        return status;
    }

    public void getNewHeartRate() {
        if (adapterBLE == null || !adapterBLE.isConnected()) {
            Log.d(TAG, " !!! Device not connected");
            return;
        }

        boolean status = adapterBLE.writeData(
                MiBandServices.UUID_SERVICE_HEARTBEAT,
                MiBandServices.UUID_HEARTRATE_CONTROL_POINT,
                MiBandServices.BYTE_NEW_HEART_RATE_SCAN
        );

        Log.d(TAG, "getNewHeartRate: status " + status);
    }

    public void getNewStepCount() {
        if (adapterBLE == null || !adapterBLE.isConnected()) {
            Log.d(TAG, " !!! Device not connected");
            return;
        }
        boolean status = adapterBLE.readData(
                MiBandServices.UUID_MIBAND1_BASE_SERVICE,
                MiBandServices.UUID_CHARACTERISTIC_STEPS
        );
        Log.d(TAG, "getNewStepCount: status " + status);
    }

    public boolean setNotificationSteps(boolean enable) {
        if (adapterBLE == null || !adapterBLE.isConnected()) {
            Log.d(TAG, " !!! Device not connected");
            return false;
        }
        boolean status = adapterBLE.setNotificationsWithDescriptor(MiBandServices.UUID_MIBAND1_BASE_SERVICE, MiBandServices.UUID_CHARACTERISTIC_STEPS, MiBandServices.UUID_NOTIFICATION_DESCRIPTOR, enable);
        Log.d(TAG, "setNotificationSteps: enable " + enable + " status " + status);
        return status;
    }

    public void setNotificationHeartRate() {
        if (adapterBLE == null || !adapterBLE.isConnected()) {
            Log.d(TAG, " !!! Device not connected");
            return;
        }
        boolean status = adapterBLE.setNotificationsWithDescriptor(
                MiBandServices.UUID_SERVICE_HEARTBEAT,
                MiBandServices.UUID_HEARTRATE_MEASUREMENT,
                MiBandServices.UUID_NOTIFICATION_DESCRIPTOR,
                true
        );
        Log.d(TAG, "setNotificationHeartRate: status " + status);
        //  adapterBLE.setNotifications(MiBandServices.UUID_SERVICE_HEARTBEAT, MiBandServices.UUID_NOTIFICATION_DESCRIPTOR);
    }

    public void setNotificationButton() {
        adapterBLE.setNotifications(
                MiBandServices.UUID_MIBAND1_BASE_SERVICE,
                MiBandServices.UUID_TOUCH_BUTTON);
    }

    public int getStepsFromByteValue(byte[] data) {
        return data[4] << 24 | (data[3] & 0xFF) << 16 | (data[2] & 0xFF) << 8 | (data[1] & 0xFF);
    }

    public int getHeartRateFromByteValue(byte[] data) {
        return (data[0] & 0xFF) << 8 | (data[1] & 0xFF);
    }
  /*  public int getCoordsFromByteValue(byte[] data) {
    int x = (((data[i2] & 255) | ((data[i2 + 1] & 255) << 8)) << 16) >> 16;
    int y = (((data[i2 + 2] & 255) | ((data[i2 + 3] & 255) << 8)) << 16) >> 16;
    int z = (((data[i2 + 4] & 255) | ((data[i2 + 5] & 255) << 8)) << 16) >> 16;
    }*/
}
