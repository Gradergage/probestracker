package device;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * @author Gradergage
 */
public class AdapterBLE {
    public static final String TAG = "AdapterBLE";

    private Context context;
    private android.os.Handler handler;
    private BluetoothDevice activeDevice; // The mi band
    private boolean isConnectedToGatt; // the gatt connection
    private BluetoothGatt bluetoothGattConnection;

    private ArrayList<OnBluetoothInteractionListener> listeners;

    public AdapterBLE(Context context, android.os.Handler handler) {
        this.context = context;
        this.handler = handler;
        this.listeners = new ArrayList<>();
        this.isConnectedToGatt = false;
    }

    public void addListener(OnBluetoothInteractionListener onBluetoothInteractionListener) {
        listeners.add(onBluetoothInteractionListener);
    }

    public void removeListener(OnBluetoothInteractionListener onBluetoothInteractionListener) {
        listeners.remove(onBluetoothInteractionListener);
    }

    public boolean isConnected() {
        return isConnectedToGatt;
    }

    /**
     * @param bluetoothAdapter current active Bluetooth adapter
     * @param name             of target device
     */
    public boolean getBluetoothDeviceByName(BluetoothAdapter bluetoothAdapter, String name) {
        Log.d(TAG, "Initialising Bluetooth connection for device (Name: " + name + ")");


        if (bluetoothAdapter.isEnabled()) {
            for (BluetoothDevice pairedDevice : bluetoothAdapter.getBondedDevices()) {
                if (pairedDevice.getName().contains(name)) {
                    Log.d(TAG, "\t Connected to: " + pairedDevice.getName() + "MAC:" + pairedDevice.getAddress());
                    activeDevice = pairedDevice;
                    return true;
                }
            }
        }
        Log.d(TAG, "\tDevices not found");
        return false;
    }

    public ArrayList<String> getDevicesList(BluetoothAdapter bluetoothAdapter) {
        ArrayList<String> list = new ArrayList<>();
        Log.d(TAG, "Searching devices");


        if (bluetoothAdapter.isEnabled()) {
            for (BluetoothDevice pairedDevice : bluetoothAdapter.getBondedDevices()) {
                list.add(pairedDevice.getName() + "*"+pairedDevice.getAddress());
            }
        }
        Log.d(TAG, "\tDevices not found");
        return list;
    }

    void getBluetoothDeviceByAddress(BluetoothAdapter bluetoothAdapter, String address) {
        Log.d(TAG, "Initialising Bluetooth connection for device (MAC: " + address + ")");


        if (bluetoothAdapter.isEnabled()) {
            for (BluetoothDevice pairedDevice : bluetoothAdapter.getBondedDevices()) {
                if (pairedDevice.getAddress().equals(address)) {
                    Log.d(TAG, "\t Connected to: " + pairedDevice.getName() + "MAC:" + pairedDevice.getAddress());
                    activeDevice = pairedDevice;
                    break;
                }
            }
        }
        Log.d(TAG, "\tDevice not found");
    }

    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, " Service discovered with status " + status);
            } else {
                Log.d(TAG, " No services discovered with status " + status);
            }
            raiseOnDiscovered();
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.d(TAG, " Gatt state: connected");
                    gatt.discoverServices();
                    isConnectedToGatt = true;
                    raiseOnConnect();
                    break;
                default:
                    Log.d(TAG, " Gatt state: not connected");
                    isConnectedToGatt = false;
                    raiseOnDisconnect();
                    break;
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        //    Log.d(TAG, " Write successful: " + Arrays.toString(characteristic.getValue()) + " UUID: " + characteristic.getUuid().toString());
            raiseOnWrite(gatt, characteristic, status);
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
          //  Log.d(TAG, " Read successful: " + Arrays.toString(characteristic.getValue()) + " UUID: " + characteristic.getUuid().toString());
            raiseOnRead(gatt, characteristic, status);
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
          //  Log.d(TAG, " Notifiaction value: " + Arrays.toString(characteristic.getValue()) + " UUID: " + characteristic.getUuid().toString());
            raiseOnNotification(gatt, characteristic);
            super.onCharacteristicChanged(gatt, characteristic);
        }
    };

    /**
     * Connecting to GATT with current context and BluetoothGattCallback object
     */
    public boolean connect() {
        if (activeDevice != null) {
            Log.d(TAG, " Establishing connection to gatt");
            bluetoothGattConnection = activeDevice.connectGatt(context, true, bluetoothGattCallback);
            if (bluetoothGattConnection==null)
            {
                return false;
            }
            else
                return true;
        } else {
            Log.d(TAG, " Cant Establish connection to gatt, device is null");
            return false;
        }
    }

    /**
     * Disconnecting from GATT
     */
    public void disconnect() {
        if (bluetoothGattConnection != null && isConnectedToGatt) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    bluetoothGattConnection.disconnect();
                    bluetoothGattConnection.close();
                    bluetoothGattConnection = null;
                    isConnectedToGatt = false;
                }
            });
        }
    }

    public BluetoothDevice getPairedDevice() {
        return activeDevice;
    }

    //Callback functions
    public void raiseOnConnect() {
        for (OnBluetoothInteractionListener listener : listeners)
            listener.onConnect();
    }

    public void raiseOnDisconnect() {
        for (OnBluetoothInteractionListener listener : listeners)
            listener.onDisconnect();
    }
    public void raiseOnDiscovered() {
        for (OnBluetoothInteractionListener listener : listeners)
            listener.onDiscovered();
    }

    public void raiseOnRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        for (OnBluetoothInteractionListener listener : listeners)
            listener.onRead(gatt, characteristic, status);
    }

    public void raiseOnWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        for (OnBluetoothInteractionListener listener : listeners)
            listener.onWrite(gatt, characteristic, status);
    }

    public void raiseOnNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        for (OnBluetoothInteractionListener listener : listeners)
            listener.onNotification(gatt, characteristic);
    }

    public boolean readData(UUID service, UUID Characteristics) {
        boolean status=false;
        if (!isConnectedToGatt || bluetoothGattConnection == null) {
            Log.d(TAG, " Read failed, BLE not initialized");
            return status;
        }
    //    Log.d(TAG, " Getting gatt service, UUID:" + service.toString());
        BluetoothGattService myGatService =
                bluetoothGattConnection.getService(service);

        if (myGatService != null) {
       //     Log.d(TAG, " Getting gatt Characteristic. UUID: " + Characteristics.toString());
            BluetoothGattCharacteristic myGatChar = myGatService.getCharacteristic(Characteristics);

            if (myGatChar != null) {
           //     Log.d(TAG, " Reading data");

                status = bluetoothGattConnection.readCharacteristic(myGatChar);
            //    Log.d(TAG, " Read status:" + status);
            }
        }
        return status;
    }

    public boolean writeData(UUID service, UUID Characteristics, byte[] value) {
        boolean status=false;

        if (!isConnectedToGatt || bluetoothGattConnection == null) {
            Log.d(TAG, " Write failed, BLE not initialized");
            return status;
        }
      //  Log.d(TAG, " Getting gatt service, UUID:" + service.toString());
        BluetoothGattService myGatService = bluetoothGattConnection.getService(service);
        if (myGatService != null) {
         //   Log.d(TAG, " Getting gatt Characteristic. UUID: " + Characteristics.toString());

            BluetoothGattCharacteristic bluetoothGattCharacteristic = myGatService.getCharacteristic(Characteristics);
            if (bluetoothGattCharacteristic != null) {
           //     Log.d(TAG, " Writing trigger");
                bluetoothGattCharacteristic.setValue(value);

                status = bluetoothGattConnection.writeCharacteristic(bluetoothGattCharacteristic);
          //      Log.d(TAG, " Writting trigger status :" + status);
            }
        }
        return status;
    }

    public void setNotifications(UUID service, UUID Characteristics) {
        if (!isConnectedToGatt || bluetoothGattConnection == null) {
            Log.d(TAG, " setNotifications failed, BLE not initialized");
            return;
        }

      //  Log.d(TAG, " Getting gatt service, UUID:" + service.toString());
        BluetoothGattService myGatService = bluetoothGattConnection.getService(service);

        if (myGatService != null) {
         //   Log.d(TAG, " Getting gatt Characteristic. UUID: " + Characteristics.toString());

            BluetoothGattCharacteristic myGatChar = myGatService.getCharacteristic(Characteristics);
            if (myGatChar != null) {
           //     Log.d(TAG, " Statring listening");
                boolean status = bluetoothGattConnection.setCharacteristicNotification(myGatChar, true);
           //     Log.d(TAG, "Set notification status :" + status);
            }
        }
    }

    public boolean setNotificationsWithDescriptor(UUID service, UUID Characteristics, UUID Descriptor, boolean enable) {
        if (!isConnectedToGatt || bluetoothGattConnection == null) {
            Log.d(TAG, " Cant get notifications from BLE, not initialized.");
            return false;
        }

      //  Log.d(TAG, " Getting gatt service, UUID:" + service.toString());
        BluetoothGattService myGatService = bluetoothGattConnection.getService(service);

        if (myGatService != null) {
         //   Log.d(TAG, " Getting gatt Characteristic. UUID: " + Characteristics.toString());

            BluetoothGattCharacteristic myGatChar = myGatService.getCharacteristic(Characteristics);
            if (myGatChar != null) {
          //      Log.d(TAG, " Statring listening");

                // second parametes is for starting\stopping th.e listener
                boolean status = bluetoothGattConnection.setCharacteristicNotification(myGatChar, true);
            //    Log.d(TAG, " Set notification status :" + status);

                BluetoothGattDescriptor myDescriptor = myGatChar.getDescriptor(Descriptor);
                if (myDescriptor != null) {
                 //  Log.d(TAG, " Writing decriptor: " + Descriptor.toString());
                    if (enable) {
                        myDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    } else {
                        myDescriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                    }
                    status = bluetoothGattConnection.writeDescriptor(myDescriptor);
                //    Log.d(TAG, " Writing decriptors result: " + status);
                    return status;
                }
            }
        }
        return false;
    }
}





