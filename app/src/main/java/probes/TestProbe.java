package probes;

/**
 * Created by Gradergage on 29.05.2018.
 */

public class TestProbe {
    public String probeName;
    public String probeDescription;

    public TestProbe(String name, String description) {
        probeName = name;
        probeDescription = description;
    }
}
