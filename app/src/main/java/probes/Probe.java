package probes;

import gradergage.probestracker.menu.TestFrameFragment;

/**
 * Created by Gradergage on 29.05.2018.
 */

public interface Probe {
    public void getResult();
    public void start();
    public void stop();
    public void finish();
    public void setFrame(TestFrameFragment frame);
}
