package probes;

import android.content.SharedPreferences;

/**
 * Created by Gradergage on 05.06.2018.
 */

public class UserParameters {
    private int height;
    private int weight;
    private int age;
    private boolean sex;

    public UserParameters(SharedPreferences pref) {

        String w = pref.getString("settings_weight", "");
        String h = pref.getString("settings_height", "");
        String a = pref.getString("settings_age", "");
        String deviceName = pref.getString("settings_device_mac", "");
        String deviceMac = pref.getString("settings_device_name", "");
        boolean s = pref.getBoolean("settings_sex", true);

        try {
            weight = Integer.parseInt(w);
            height = Integer.parseInt(h);
            age = Integer.parseInt(a);
            sex = s;
        } catch (java.lang.RuntimeException e) {
            e.printStackTrace();
        }
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public boolean getSex() {
        return sex;
    }
}
