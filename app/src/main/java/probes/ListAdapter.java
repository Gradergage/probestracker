package probes;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import gradergage.probestracker.R;

/**
 * Created by Gradergage on 29.05.2018.
 */

public class ListAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<TestProbe> probesList;

    public ListAdapter(Context context, ArrayList<TestProbe> probes) {
        ctx = context;
        probesList = probes;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    // кол-во элементов
    @Override
    public int getCount() {
        return probesList.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return probesList.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.listview_home, parent, false);
        }

      //  TestProbe p = getProduct(position);
     //   ((TextView) view.findViewById(R.id.probeName)).setText(p.probeName);
       // ((TextView) view.findViewById(R.id.probeDesc)).setText(p.probeDescription);
        return view;
    }
}