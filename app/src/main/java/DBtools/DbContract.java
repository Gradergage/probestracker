package DBtools;

/**
 * Created by Gradergage on 21.05.2018.
 */

public class DbContract {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ProbesTrackerDB";
    public static final String TEXT_TYPE = " TEXT";
    public static final String COMMA_SEP = ",";
    public static final String TABLE_NAME_PROBES = "probes";
    public static final String TABLE_NAME_PROBES_LOG = "probes_log";
    public static final String TABLE_NAME_HEARTRATE = "pulse_log";
    public static final String TABLE_NAME_STEPS = "steps_log";
}
