package DBtools;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Random;

/**
 * Created by Gradergage on 21.05.2018.
 */

public class DbHelper extends SQLiteOpenHelper {
    private final String TAG = "DbHelper";

    public DbHelper(Context context) {
        super(context, DbContract.DATABASE_NAME, null, DbContract.DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL("drop table if exists probes;");
        createNewDb(db);

    }

    public void createNewDb(SQLiteDatabase db) {
        Log.d(TAG, "* Сreating database tables");
     /*   // создаем таблицу с полями
        db.execSQL("drop table if exists probes ");
*/

        db.execSQL("create table "+ DbContract.TABLE_NAME_PROBES+" ("
                + "id integer primary key autoincrement,"
                + "name text,"
                + "dynamics text" +
                ");");

   //     db.execSQL("drop table if exists probes_log;");
        db.execSQL("create table probes_log ("
                + "id integer primary key autoincrement,"
                + "probe_id integer,"
                + "result real,"
                + "result_text text,"
                + "time text,"
                + "FOREIGN KEY(probe_id) REFERENCES probes(id)"
                + ");");

        db.execSQL("create table pulse_log ("
                + "id integer primary key autoincrement,"
                + "value integer,"
                + "time text"
                + ");");

        db.execSQL("create table steps_log ("
                + "id integer primary key autoincrement,"
                + "value integer,"
                + "time text"
                + ");");

       /* db.execSQL("INSERT into probes (name,dynamics) values('Cooper',0);");
        db.execSQL("INSERT into probes (name,dynamics) values('Roofie',0);");
        db.execSQL("INSERT into probes (name,dynamics) values('UUK',0);");
        db.execSQL("INSERT into probes (name,dynamics) values('Harvard',0);");*/
     //  loadDbFirstTime(db);
    }

    public void clearDb(SQLiteDatabase db)

    {
        db.execSQL("drop table if exists steps_log;");
        db.execSQL("drop table if exists pulse_log;");
        db.execSQL("drop table if exists probes_log;");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void loadDbFirstTime(SQLiteDatabase db) {
        db.execSQL("delete from " + DbContract.TABLE_NAME_PROBES + ";");
        db.execSQL("delete from sqlite_sequence where name='probes'");
        putProbe(db,"Cooper");
        putProbe(db,"Harvard");
        putProbe(db,"UKK");
        putProbe(db,"Ruffier");

    }
    public void addFakeData(SQLiteDatabase db)
    {
        Random rnd = new Random();
        putProbeResult(db,1,rnd.nextDouble(),"Perfect","2018-10-13 18:35");
        putProbeResult(db,2,rnd.nextDouble(),"Good","2018-10-13 18:35");
        putProbeResult(db,3,rnd.nextDouble(),"Satisfactorily","2018-10-13 18:35");
        putProbeResult(db,4,rnd.nextDouble(),"Bad","2018-10-13 18:33");
        putProbeResult(db,2,rnd.nextDouble(),"Very bad","2018-10-13 18:35");
    }
    public void putProbe(SQLiteDatabase db, String name) {
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        db.insert(DbContract.TABLE_NAME_PROBES, null, cv);
    }
    public void putProbeResult(SQLiteDatabase db,int probeId,double result, String textResult,String time)
    {
      //  ContentValues cv = new ContentValues();
      //  cv.put("probe_id", probeId);
     //   cv.put("result", result);
      //  cv.put("result_text", textResult);
      //  cv.put("time", time);
        db.execSQL("INSERT INTO probes_log (probe_id,result,result_text,time) VALUES (" + probeId + ","+result+",'"+textResult+"', datetime('now'));");
    }
    public void drop(SQLiteDatabase db)
    {
      //  db.execSQL("drop sqlite_sequence  if exists probes_log;");
        db.execSQL("drop table if exists probes_log;");
    }
}

