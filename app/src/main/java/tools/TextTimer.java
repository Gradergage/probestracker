package tools;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by Gradergage on 07.06.2018.
 */

public class TextTimer {

    private TextView clock;
    private Handler handler;
    private int cntTime;
    private CountDownTimer timer;
    private TimerListener parent;
    private int seconds;
    private int minutes;
    private boolean inv;

    public TextTimer(TextView textView, Handler activityHandler, int countTime, final TimerListener view, boolean inverse) {
        clock = textView;
        this.cntTime = countTime;
        this.handler = activityHandler;
        this.parent = view;
        inv = inverse;
        seconds = 0;
        minutes = cntTime / 60 / 1000;
        timer = new CountDownTimer(cntTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                setTimeText(millisUntilFinished);

            }

            public void setTimeText(long time) {
                final int secs;
                if (inv)
                    secs = (int) ((cntTime - time) / 1000);
                else
                    secs = (int) (time / 1000);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        parent.onSecond();
                    }
                });
                final int mins = secs / 60;
                final int realSecs = secs - mins * 60;
                seconds = realSecs;
                if (mins != minutes) {
                    minutes = mins;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            parent.onMinute();
                        }
                    });
                }
                if (clock != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            clock.setText("" + String.format("%02d", mins) + ":" + String.format("%02d", realSecs));
                        }
                    });
                }

            }

            @Override
            public void onFinish() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (clock != null) {
                            clock.setText("00:00");
                        }
                        parent.onFinish();
                    }
                });
            }
        };
    }

    public void start() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (clock != null) {
                        clock.setText("00:00");
                    }
                }
            });
            Log.d("Timer", "innerTimer started");
            timer.start();
    }

    public int getSeconds() {
        return seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public void stop() {
        timer.cancel();
    }
}
