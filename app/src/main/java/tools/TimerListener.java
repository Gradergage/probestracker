package tools;

/**
 * Created by Gradergage on 07.06.2018.
 */

public interface TimerListener {
    public void onSecond();
    public void onMinute();
    public void onFinish();
}
