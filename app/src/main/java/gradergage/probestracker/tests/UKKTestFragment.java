package gradergage.probestracker.tests;


import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.UUID;

import device.MiBandServices;
import gradergage.probestracker.R;
import gradergage.probestracker.menu.MenuFragment;
import gradergage.probestracker.menu.TestFrameFragment;
import probes.Probe;
import tools.TextTimer;
import tools.TimerListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class UKKTestFragment extends MenuFragment implements Probe, TimerListener {
    private static final String TAG = "UKKTestFragment";
    private int t1;
    private int t2;
    private int W;
    private final int PULSE_MEASUREMENT_COUNT = 4;
    private boolean testStarted = false;
    private int stepsCorrection = 0;
    private int steps;
    private int pulseCounter = 0;
    private int[] pulseStack = new int[PULSE_MEASUREMENT_COUNT];
    private final int DISTANCE_LIMIT_COMPLETE = 10;
    private TextTimer timer;
    private TextView clock;
    private TextView pulse;
    private TextView distance;
    private TextView pulseInfo;
    private TextView distanceInfo;
    TestFrameFragment frame;
    private boolean isMeasuring = false;
    private final int TIME_LIMIT_COMPLETE = 60 * 60 * 1000; //one hour limit

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ukk, container, false);
        clock = view.findViewById(R.id.text_clock);
        pulse = view.findViewById(R.id.text_ukk_pulse);
        distance = view.findViewById(R.id.text_ukk_distance);
        pulseInfo = view.findViewById(R.id.text_ukk_pulse_info);
        distanceInfo = view.findViewById(R.id.text_ukk_distance_info);
        return view;
    }

    @Override
    public void getResult() {

        //    stop();
        W = 0;
        String textResult = null;
        for (int p : pulseStack) {
            W += p;
        }
        W = W / PULSE_MEASUREMENT_COUNT;
        handler.post(new Runnable() {
            @Override
            public void run() {
                pulse.setText("" + W);
                pulseInfo.setText(R.string.info_pulse);
            }
        });

        double result;
        if (user.getSex()) //for men
            result = 420 - (11.6 * t1 + 0.2 * t2 + 0.56 * W + 2.6 * user.getWeight() / (user.getHeight() * user.getHeight() / 10000) - 0.2 * user.getAge());
        else               //for woman
            result = 304 - (8.5 * t1 + 0.14 * t2 + 0.32 * W + 1.1 * user.getWeight() / (user.getHeight() * user.getHeight() / 10000) - 0.4 * user.getAge());

        int[] marks = getResources().getIntArray(R.array.res_ukk);
        String[] marksText = getResources().getStringArray(R.array.mark_text);
        for (int i = 0; i < marks.length; i++) {
            if (result < marks[i]) {
                textResult = marksText[i + 1];
            } else {
                textResult = marksText[i];
                break;
            }
        }
        testStarted = false;
        trackerAdapter.setNotificationSteps(false);
        Log.d(TAG, "getResult: GOT RESULT " + textResult);
        frame.showResult(textResult,result);
    }

    @Override
    public void start() {
        if (!testStarted) {
            testStarted = true;
            steps = 0;
            pulseCounter = 0;
            stepsCorrection = 0;
            timer = new TextTimer(clock, handler, TIME_LIMIT_COMPLETE, this, true);
            timer.start();
            // trackerAdapter.getNewStepCount();
            stepsCorrection = mainActivity.getStepsCorrection();
            steps = stepsCorrection;
            trackerAdapter.setNotificationSteps(true);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    distance.setText("0 " + getString(R.string.info_meters));
                    pulse.setText("0");
                    distanceInfo.setText(R.string.info_distance_measuring);
                }
            });
        }

    }

    @Override
    public void stop() {
        trackerAdapter.setNotificationSteps(false);
        if(timer!=null)
            timer.stop();
    }

    @Override
    public void finish() {
        timer.stop();
        t2 = timer.getSeconds();
        t1 = timer.getMinutes();
    }

    @Override
    public void setFrame(TestFrameFragment frame) {
        this.frame = frame;
    }


    @Override
    public void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        if (testStarted) {
            final UUID readUUID = characteristic.getUuid();
            if (readUUID.equals(MiBandServices.UUID_HEARTRATE_MEASUREMENT)) {
                final int heartrate = trackerAdapter.getHeartRateFromByteValue(characteristic.getValue());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pulse.setText("" + heartrate);
                    }
                });

                if (pulseCounter < PULSE_MEASUREMENT_COUNT) {
                    if (heartrate != 0) {
                        pulseStack[pulseCounter] = heartrate;
                        pulseCounter++;
                    }
                    trackerAdapter.getNewHeartRate();
                    Log.d(TAG, "onNotification: GOT HEARTRATE " + pulseCounter + " TIMES");
                } else {
                    isMeasuring = false;
                    getResult();
                }

            } else if (readUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
                if (getDistance() >= DISTANCE_LIMIT_COMPLETE) {
                    finish();
                    isMeasuring = true;
                    //  trackerAdapter.setNotificationSteps(false);
                    //measureTimer
                   /* Looper.prepare();
                    timer = new TextTimer(null, handler, TIME_LIMIT_COMPLETE, this, true);
                    timer.start();
                    Looper.loop();*/
                    steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            distanceInfo.setText(R.string.info_distance);
                            pulseInfo.setText(R.string.info_pulse_measuring);
                        }
                    });

                    trackerAdapter.getNewHeartRate();
                } else {
                    steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            distance.setText("" + (int) getDistance() + " " + getString(R.string.info_meters));
                        }
                    });
                }
            }
        }
    }

    public double getDistance() {
        return getSteps() * (1.0 * user.getHeight() / 2.0 / 100.0);
    }

    public int getSteps() {
        return steps - stepsCorrection;
    }

    @Override
    public void onSecond() {
       /* Log.d(TAG, "onSecond: counter works");
        if(isMeasuring)
        {
            trackerAdapter.setContiniousHeartrate(true);
        }*/
    }
}
