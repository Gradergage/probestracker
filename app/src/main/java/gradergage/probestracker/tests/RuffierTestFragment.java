package gradergage.probestracker.tests;


import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.UUID;

import device.MiBandServices;
import gradergage.probestracker.R;
import gradergage.probestracker.menu.MenuFragment;
import gradergage.probestracker.menu.TestFrameFragment;
import probes.Probe;
import tools.TextTimer;


/**
 * A simple {@link Fragment} subclass.
 */
public class RuffierTestFragment extends MenuFragment implements Probe {


    private TextTimer timer;
    private TextView clock;
    private TextView pulse1;
    private TextView pulse2;
    private TextView pulse3;

    private TextView info;
    private TextView pulseInfo;
    private TextView distanceInfo;
    private final int PULSE_MEASUREMENT_COUNT = 4;
    private int[] pulseStack = new int[PULSE_MEASUREMENT_COUNT];

    TestFrameFragment frame;
    private final int TIME_LIMIT_COMPLETE = 15 * 1000;

    private int p1 = 0;
    private int p2 = 0;
    private int p3 = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ruffier, container, false);
        clock = view.findViewById(R.id.text_clock);
        pulse1 = view.findViewById(R.id.text_roofie_pulse);
        pulse2 = view.findViewById(R.id.text_roofie_pulse2);
        pulse3 = view.findViewById(R.id.text_roofie_pulse3);
        pulseInfo = view.findViewById(R.id.text_roofie_pulse_info);
        info = view.findViewById(R.id.task_panel);
        return view;
    }

    @Override
    public void getResult() {

        double result = ((p2 - 70) + 2 * (p3 - p1)) / 10.0;
        handler.post(new Runnable() {
            @Override
            public void run() {
                info.setText("");
            }
        });
        String textResult=null;
        int[] marks = getResources().getIntArray(R.array.res_roofie_advanced);
        String[] marksText = getResources().getStringArray(R.array.mark_text);
        for (int i = 0; i < marks.length; i++) {
            textResult = marksText[i + 1];
            if (result < marks[i]) {
                break;
            }
        }
        if(result<0)
            result=-result;
        frame.showResult(textResult,result);
    }

    @Override
    public void start() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                pulseInfo.setText(R.string.info_pulse_measuring);
                pulse1.setText("0");
                pulse2.setText("0");
                pulse3.setText("0");
                info.setText(R.string.info_wait);
            }
        });
        p1=0;
        p2=0;
        p3=0;
        timer = new TextTimer(clock, handler, TIME_LIMIT_COMPLETE, this, false);
        //  trackerAdapter.getNewHeartRate();
        trackerAdapter.setContiniousHeartrate(true);

        // trackerAdapter.setContiniousHeartrate(true);


    }

    @Override
    public void stop() {
        trackerAdapter.setContiniousHeartrate(false);
        if(timer!=null)
            timer.stop();
    }

    @Override
    public void finish() {

    }

    @Override
    public void onFinish() {
        trackerAdapter.setContiniousHeartrate(true);
        handler.post(new Runnable() {
            @Override
            public void run() {
                pulseInfo.setText(R.string.info_pulse_measuring);
                info.setText(R.string.info_wait);
            }
        });
        if (p2 == 0) {
            timer.start();
        }
    }

    @Override
    public void setFrame(TestFrameFragment frame) {
        this.frame = frame;
    }

    @Override
    public void onDisconnect() {

    }

    @Override
    public void onConnect() {

    }

    @Override
    public void onDiscovered() {

    }

    @Override
    public void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        final UUID readUUID = characteristic.getUuid();
        if (readUUID.equals(MiBandServices.UUID_HEARTRATE_MEASUREMENT)) {
            final int heartrate = trackerAdapter.getHeartRateFromByteValue(characteristic.getValue());

            if (p1 == 0) {
                p1 = heartrate;
                trackerAdapter.setContiniousHeartrate(false);
                timer.start();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pulseInfo.setText(R.string.info_pulse);
                        pulse1.setText("" + p1);
                        info.setText(R.string.info_exercises);
                    }
                });


            } else if (p2 == 0) {
                p2 = heartrate;
                trackerAdapter.setContiniousHeartrate(false);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pulseInfo.setText(R.string.info_pulse);
                        pulse2.setText("" + p2);
                    }
                });

            } else if (p3 == 0) {
                p3 = heartrate;
                trackerAdapter.setContiniousHeartrate(false);
                timer.stop();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pulseInfo.setText(R.string.info_pulse);
                        pulse3.setText("" + p3);
                    }
                });
                getResult();
            }
        }
    }
}
