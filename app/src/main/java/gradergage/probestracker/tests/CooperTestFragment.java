package gradergage.probestracker.tests;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.UUID;

import device.MiBandServices;
import gradergage.probestracker.R;
import gradergage.probestracker.menu.MenuFragment;
import gradergage.probestracker.menu.TestFrameFragment;
import probes.Probe;
import tools.TextTimer;
import tools.TimerListener;


public class CooperTestFragment extends MenuFragment implements Probe, TimerListener {
    private final String TAG = "CooperTestFragment";
    private View view;
    private int stepsCorrection = 0;
    private int steps;
    private int mark;
    private TextTimer timer;
    private TextView textTimer;
    private TextView distance;
    private int TIME_COOPER_TEST = 12 * 60 * 1000; // 12 minutes in milliseconds
    private boolean testStarted = false;
    TestFrameFragment frame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Created");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_cooper, container, false);
        textTimer = (TextView) view.findViewById(R.id.text_clock);
        distance = (TextView) view.findViewById(R.id.text_cooper_distance);
        timer = new TextTimer(textTimer, handler, TIME_COOPER_TEST, this, false);
        adapterBLE.addListener(this);
        Log.d(TAG, "View Created");
        return view;

    }

    @Override
    public void onRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        final UUID readUUID = characteristic.getUuid();

        if (readUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
            final int steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
            if (stepsCorrection == 0) {
                stepsCorrection = steps;
                trackerAdapter.setNotificationSteps(true);
            }
        }
    }

    @Override
    public void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        if (testStarted) {
            final UUID readUUID = characteristic.getUuid();
            if (readUUID.equals(MiBandServices.UUID_HEARTRATE_MEASUREMENT)) {
                final int heartrate = trackerAdapter.getHeartRateFromByteValue(characteristic.getValue());
                Log.d(TAG, " * Got heartrate: " + heartrate);
            } else if (readUUID.equals(MiBandServices.UUID_TOUCH_BUTTON)) {
                Log.d(TAG, " * Got button touch");
            } else if (readUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
                steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        distance.setText("" + (int) getDistance() + " " + getString(R.string.info_meters));

                    }
                });
                mainActivity.writeStepsToDb(steps);
            }
        }
    }

    @Override
    public void getResult() {
        if (isAdded()) {
            int[] marks = getResources().getIntArray(R.array.mark);
            int[] ages = getResources().getIntArray(R.array.ages);
            int id = 0;
            for (int i = 0; i < ages.length; i++) {
                if (user.getAge() <= ages[i]) {

                    if (user.getSex()) {
                        id = getResources().getIdentifier("res_" + i + "_m", "array", view.getContext().getPackageName());

                    } else {
                        id = getResources().getIdentifier("res_" + i + "_f", "array", view.getContext().getPackageName());
                    }
                    break;
                }
            }
            int[] criterion = getResources().getIntArray(id);
            for (int i = 0; i < criterion.length; i++) {
                if (getDistance() > criterion[i]) {
                    mark = marks[i];
                    break;
                }
            }
        }
    }

    public double getDistance() {
        double distance = getSteps() * (1.0 * user.getHeight() / 2.0 / 100.0);
        return distance;
    }

    public int getSteps() {
        return steps - stepsCorrection;
    }

    @Override
    public void start() {
        steps = 0;
        stepsCorrection = 0;
        if (!testStarted) {
            timer.start();
            testStarted = true;
            trackerAdapter.getNewStepCount();
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                distance.setText("0 " + getString(R.string.info_meters));
            }
        });
    }

    @Override
    public void stop() {
        testStarted = false;
        trackerAdapter.setNotificationSteps(false);
        if(timer!=null)
            timer.stop();
    }

    @Override
    public void onFinish() {
        finish();
    }

    public void finish() {
        stop();
        getResult();
        Log.d(TAG, "stop: " + mark);
        String[] marks = getResources().getStringArray(R.array.mark_text);
        String res = marks[marks.length - mark - 1];
        frame.showResult(res,mark);
    }

    @Override
    public void setFrame(TestFrameFragment frame) {
        this.frame = frame;
    }

    @Override
    public void onSecond() {
        //  trackerAdapter.getNewStepCount();
    }

}
