package gradergage.probestracker.tests;


import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.UUID;

import device.MiBandServices;
import gradergage.probestracker.R;
import gradergage.probestracker.menu.MenuFragment;
import gradergage.probestracker.menu.TestFrameFragment;
import probes.Probe;
import tools.TextTimer;
import tools.TimerListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class HarvardTestFragment extends MenuFragment implements Probe, TimerListener {
    private final String TAG = "HarvardTestFragment";
    private TextTimer timer;
    private TextView clock;
    private TextView pulse1;
    private TextView pulse2;
    private TextView pulse3;
    private int minutes;
    private int seconds;
    private int stepsCorrection;
    private TextView info;
    private TextView pulseInfo;
    private TextView distanceInfo;
    private int steps = 0;
    private boolean relaxPhase = false;
    private final int PULSE_MEASUREMENT_COUNT = 4;
    private int[] pulseStack = new int[PULSE_MEASUREMENT_COUNT];
    TestFrameFragment frame;
    private final int TIME_LIMIT_COMPLETE = 5 * 60 * 1000;
    private final int TIME_LIMIT_RELAX = 5 * 60 * 1000;
    private int p1;
    private int p2;
    private int p3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_harvard, container, false);
        clock = view.findViewById(R.id.text_clock);
        pulse1 = view.findViewById(R.id.text_roofie_pulse);
        pulse2 = view.findViewById(R.id.text_roofie_pulse2);
        pulse3 = view.findViewById(R.id.text_roofie_pulse3);
        pulseInfo = view.findViewById(R.id.text_roofie_pulse_info);
        info = view.findViewById(R.id.task_panel);
        return view;
    }

    @Override
    public void getResult() {

        if (minutes != 0 && seconds != 0)
            seconds += minutes * 60;
        else if (minutes != 0 && seconds == 0)
            seconds = minutes * 60;
        seconds = seconds + minutes * 60;
        double result = 100 * seconds / (2 * (p1 + p2 + p3));
        String textResult = "";
        int[] marks = getResources().getIntArray(R.array.res_harvard);
        String[] marksText = getResources().getStringArray(R.array.mark_text);
        for (int i = 0; i < marks.length; i++) {
            textResult = marksText[i];
            if (result > marks[i]) {
                break;
            }
        }
        frame.showResult(textResult, result);
    }

    @Override
    public void start() {
        timer = new TextTimer(clock, handler, TIME_LIMIT_COMPLETE, this, true);
        p1 = 0;
        p2 = 0;
        p3 = 0;
        relaxPhase = false;
        stepsCorrection = mainActivity.getStepsCorrection();
        steps = stepsCorrection;
        handler.post(new Runnable() {
            @Override
            public void run() {
                pulseInfo.setText(R.string.info_pulse);
                info.setText(R.string.info_exercises);
            }
        });
        //trackerAdapter.setNotificationSteps(true);
        timer.start();
        trackerAdapter.getNewStepCount();
    }

    @Override
    public void stop() {
        if (timer != null)
            timer.stop();
        trackerAdapter.setNotificationSteps(false);
    }

    @Override
    public void finish() {

    }

    @Override
    public void setFrame(TestFrameFragment frame) {
        this.frame = frame;
    }

    @Override
    public void onMinute() {
        Log.d(TAG, "onMinute: " + timer.getMinutes() + ":" + timer.getSeconds());
        if (relaxPhase) {
            if (timer.getMinutes() == 1 || timer.getMinutes() == 2 || timer.getMinutes() == 3) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        // pulseInfo.setText(R.string.info_pulse);
                        pulseInfo.setText(R.string.info_pulse_measuring);
                    }
                });
                Log.d(TAG, "onMinute: starting measurement P" + timer.getMinutes());
                trackerAdapter.getNewHeartRate(); //  trackerAdapter.setContiniousHeartrate(true);
                //trackerAdapter.setContiniousHeartrate(true);
            }
        }
    }

    @Override
    public void onSecond() {
        // Log.d(TAG, "onSecond:");
        if (!relaxPhase) trackerAdapter.getNewStepCount();
    }

    @Override
    public void onFinish() {
        trackerAdapter.setNotificationSteps(false);
        handler.post(new Runnable() {
            @Override
            public void run() {
                // pulseInfo.setText(R.string.info_pulse);
                info.setText(R.string.info_wait);
            }
        });
        if (!relaxPhase) {  //Got 1'st phase complete time
            minutes = timer.getMinutes();
            seconds = timer.getSeconds();
        }
        Log.d(TAG, "onFinish: 1st phase finished " + minutes + ":" + seconds);
        relaxPhase = true;
        //timer = new TextTimer(clock, handler, TIME_LIMIT_RELAX, this, true);
        timer.start();
    }

    @Override
    public void onRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        super.onRead(gatt, characteristic, status);
        final UUID readUUID = characteristic.getUuid();
        if (readUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
            steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
            int seconds = timer.getSeconds() + timer.getMinutes() * 60;
            //   if (timer.getMinutes() != 0 ) {
            double coef = 1.0 * getSteps() / seconds;
            Log.d(TAG, "onNotification: " + coef + " steps " + getSteps());
            if (seconds > 15) {
                if (coef < 0.6) {
                    timer.stop();
                    //  relaxPhase=true;
                    onFinish();
                } else if (coef < 0.75) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            // pulseInfo.setText(R.string.info_pulse);
                            info.setText(R.string.info_harvard_faster);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            // pulseInfo.setText(R.string.info_pulse);
                            info.setText(R.string.info_exercises);
                        }
                    });

                }
            }
        }
    }

    @Override
    public void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        final UUID readUUID = characteristic.getUuid();
        if (readUUID.equals(MiBandServices.UUID_HEARTRATE_MEASUREMENT)) {
            final int heartrate = trackerAdapter.getHeartRateFromByteValue(characteristic.getValue());
            if (relaxPhase) {
                if (p1 == 0 && timer.getMinutes() == 1) {
                    p1 = heartrate;
                    Log.d(TAG, "onNotification: P1" + heartrate + " " + timer.getMinutes() + ":" + timer.getSeconds());
                    // trackerAdapter.setContiniousHeartrate(false);
                    trackerAdapter.getNewHeartRate();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            // pulseInfo.setText(R.string.info_pulse);
                            pulseInfo.setText(R.string.info_pulse);
                            pulse1.setText("" + p1);
                        }
                    });
                } else if (p2 == 0 && timer.getMinutes() == 2) {
                    p2 = heartrate;
                    //   trackerAdapter.setContiniousHeartrate(false);
                    Log.d(TAG, "onNotification: P2 " + heartrate + " " + timer.getMinutes() + ":" + timer.getSeconds());
                    trackerAdapter.getNewHeartRate();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            // pulseInfo.setText(R.string.info_pulse);
                            pulseInfo.setText(R.string.info_pulse);
                            pulse2.setText("" + p2);
                        }
                    });
                } else if (p3 == 0 && timer.getMinutes() == 3) {
                    p3 = heartrate;
                    Log.d(TAG, "onNotification: P3 " + heartrate + " " + timer.getMinutes() + ":" + timer.getSeconds());
                    // trackerAdapter.setContiniousHeartrate(false);
                    trackerAdapter.getNewHeartRate();
                    timer.stop();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            // pulseInfo.setText(R.string.info_pulse);
                            pulseInfo.setText(R.string.info_pulse);
                            pulse3.setText("" + p3);
                            clock.setText("" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                        }
                    });
                    getResult();
                }
            }
        } else if (readUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
           /* steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
            int seconds = timer.getSeconds() + timer.getMinutes() * 60;
            //   if (timer.getMinutes() != 0 ) {
            double coef = 1.0 * getSteps() / seconds;
            Log.d(TAG, "onNotification: " + coef + " steps" + getSteps());
            if (coef < 0.6) {
                timer.stop();
                //  relaxPhase=true;
                onFinish();
            } else if (coef < 0.75) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        // pulseInfo.setText(R.string.info_pulse);
                        info.setText(R.string.info_harvard_faster);
                    }
                });
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        // pulseInfo.setText(R.string.info_pulse);
                        info.setText(R.string.info_exercises);
                    }
                });

            }*/
            //  }
        }

    }

    public int getSteps() {
        return steps - stepsCorrection;
    }
}
