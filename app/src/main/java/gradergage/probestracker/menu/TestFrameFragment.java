package gradergage.probestracker.menu;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;

import DBtools.DbContract;
import device.OnBluetoothInteractionListener;
import gradergage.probestracker.R;
import gradergage.probestracker.activities.MainActivity;
import probes.Probe;
import probes.UserParameters;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestFrameFragment extends MenuFragment {


    private final String TAG = "TestFrameFragment";
    private ConstraintLayout probeLayout;
    private UserParameters user;
    private Probe test;
    private SharedPreferences pref;
    private Button btnStart;
    private Button btnInfo;
    private Button btnBack;
    private View view;
    private TextView probeName;
    private TextView probeResult;
    private String currentProbe;
    private int probeId;
    private Object probeInstance;
    private InfoMenuFragment info;
    private ArrayList<Map<String, Object>> probesList;
    private boolean infoActive = false;
    private MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setProbeName(String name,int id) {
        currentProbe = name;
        probeId=id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_test_frame, container, false);
        probeName = (TextView) view.findViewById(R.id.text_probe_name);
        probeResult = (TextView) view.findViewById(R.id.text_probe_result);
        probeLayout = (ConstraintLayout) view.findViewById(R.id.layout_probe_content);

        btnStart = (Button) view.findViewById(R.id.btn_probe_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start();
            }
        });
        handler = new Handler(Looper.getMainLooper());
        btnInfo = (Button) view.findViewById(R.id.btn_probe_info);
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                info();
            }
        });
        btnBack = (Button) view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        back();
                        ((MainActivity) getActivity()).getNavigation().setSelectedItemId(R.id.navigation_probes);
                    }
                });

            }
        });
        setContent(currentProbe);
        return view;
    }

    private void setContent(String name) {
        probeInstance = null;

        try {
            Class<?> c = Class.forName(getClassNameFromProbe(name));
            //   Log.d(TAG, "Class found = " + c.getName());
            // Log.d(TAG, "Package = " + c.getPackage());
            Constructor<?> ctor = c.getConstructor();
            probeInstance = ctor.newInstance();
            test = (Probe) probeInstance;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if (probeInstance != null) {
            test.setFrame(this);
            info = new InfoMenuFragment();
            info.setText(currentProbe);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.layout_probe_content, (android.support.v4.app.Fragment) probeInstance);
            fragmentTransaction.commit();
            adapterBLE.addListener((OnBluetoothInteractionListener) test);
            //this.getResources().getIdentifier("nameOfDrawable", "string", this.getPackageName());
            probeName.setText(getString(this.getResources().getIdentifier("probe_name_" + name.toLowerCase(), "string", this.getActivity().getPackageName())));

        } else {
            Log.d(TAG, "setContent:" + false);
        }
    }

    public String getClassNameFromProbe(String name) {
        return "gradergage.probestracker.tests." + name + "TestFragment";
    }

    public void start() {
        test.start();
        probeResult.setText("");
    }

    public void back() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (!infoActive) {
            adapterBLE.removeListener((OnBluetoothInteractionListener) test);
            test.stop();

            fragmentTransaction.remove((android.support.v4.app.Fragment) test);
            fragmentTransaction.commit();
        } else {
            fragmentTransaction.remove((android.support.v4.app.Fragment) info);
            fragmentTransaction.commit();
        }
    }

    public void info() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (!infoActive) {
            fragmentTransaction.replace(R.id.layout_probe_content, info);
            fragmentTransaction.commit();
            infoActive = true;
        } else {
            fragmentTransaction.replace(R.id.layout_probe_content, (android.support.v4.app.Fragment) probeInstance);
            fragmentTransaction.commit();
            infoActive = false;
        }
    }

    public void showResult(String res, final double index) {

        final String result = res;
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (isAdded())
                    probeResult.setText(result + ", " + currentProbe + " index: " + index);
            }
        });

        db.execSQL("INSERT INTO " + DbContract.TABLE_NAME_PROBES_LOG + " (probe_id,result,result_text,time) VALUES (" + probeId + ","+index+",'"+res+"', datetime('now'));");

    }
}
