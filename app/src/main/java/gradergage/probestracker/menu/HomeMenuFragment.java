package gradergage.probestracker.menu;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import DBtools.DbContract;
import device.MiBandServices;
import gradergage.probestracker.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * create an instance of this fragment.
 */
public class HomeMenuFragment extends MenuFragment {// Fragment implements OnBluetoothInteractionListener {// Fragment implements OnBluetoothInteractionListener {

    private static final String TAG = "HomeMenuFragment";
    // private boolean isConnectedToDevice = false;

    private View view;
    final String ATTRIBUTE_NAME_PROBENAME = "probename";
    final String ATTRIBUTE_NAME_PROBEDATE = "date";
    final String ATTRIBUTE_NAME_PROBERESULT = "result";
    final String ATTRIBUTE_NAME_ID = "id";
    SimpleAdapter simpleAdapter;
    private TextView stepsCountView;
    private TextView pulseCountView;
    ImageView picSteps;
    ImageView picHeartrate;
    private BarChart chart;
    private boolean hrActive = false;
    private boolean stepsActive = false;
    private ListView listViewProbesResults;
    ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
    int counter = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_menu_home, container, false);
        stepsCountView = view.findViewById(R.id.steps_count);
        pulseCountView = view.findViewById(R.id.pulse_count);
        chart = view.findViewById(R.id.chart);
        chart.setDrawGridBackground(false);
        chart.setDrawBorders(false);
        chart.setTouchEnabled(false);
        chart.setScaleEnabled(false);
        chart.setDescription(new Description());
        chart.setDoubleTapToZoomEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setDrawAxisLine(false);
        chart.getXAxis().setEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxis(YAxis.AxisDependency.LEFT);
        chart.getAxisLeft().setAxisMinimum(0f);
        chart.getAxisLeft().setEnabled(false);//.setDrawGridLines(false);
        chart.setScaleEnabled(false);
        chart.setPinchZoom(false);
       // chart.setFitBars(true);
       // chart.setViewPortOffsets(0f, 0f, 0f, -20f);
     //   chart.setViewPortOffsets(0f, -20f, 0f, 20f);
      //  chart.setScaleYEnabled(false);
        chart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        chart.getLegend().setEnabled(false);
        listViewProbesResults = view.findViewById(R.id.lv_probes_result);

        picSteps = view.findViewById(R.id.step_img);
        picSteps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation anim = null;
                anim = AnimationUtils.loadAnimation(getContext(), R.anim.heartbeat);
                if(trackerAdapter.setNotificationSteps(stepsActive));
                stepsActive = !stepsActive;
                if(stepsActive)
                    picSteps.startAnimation(anim);
                else
                    picSteps.clearAnimation();

            }
        });

        Log.d(TAG, "DbHelper: " + getContext().getDatabasePath(DbContract.DATABASE_NAME));
        picHeartrate = view.findViewById(R.id.heartrate_img);
        picHeartrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = 0;
                Animation anim = null;
                anim = AnimationUtils.loadAnimation(getContext(), R.anim.heartbeat);

                if(trackerAdapter.setContiniousHeartrate(!hrActive));
                hrActive = !hrActive;
                if(hrActive)
                    picHeartrate.startAnimation(anim);
                else
                    picHeartrate.clearAnimation();




            }
        });

        readProbeResults();

        String[] from = {ATTRIBUTE_NAME_PROBENAME, ATTRIBUTE_NAME_PROBERESULT, ATTRIBUTE_NAME_PROBEDATE, ATTRIBUTE_NAME_ID};
        int[] to = {R.id.probe_name, R.id.probe_result, R.id.probe_time, R.id.probe_id};
        simpleAdapter = new SimpleAdapter(view.getContext(), data, R.layout.listview_home,
                from, to);
        MyViewBinder newBinder = new MyViewBinder();
        simpleAdapter.setViewBinder(newBinder);
        listViewProbesResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View listView, int i, long l) {

                Log.d(TAG, "itemSelect: position = " + i + ", id = "
                        + l);
                TextView text = (TextView) listView.findViewById(R.id.probe_id);
                TextView text2 = (TextView) listView.findViewById(R.id.probe_name);
                String id = text.getText().toString();
                Log.d(TAG, "onItemClick: "+id+" "+text2);

                LinearLayout ll=view.findViewById(R.id.layout_list_probe);
              //  ll.setBackgroundColor(getResources().getColor(R.color.colorOrange));
                redrawGraph(id);
            }
        });
        listViewProbesResults.setAdapter(simpleAdapter);

        // dbExport();
        return view;

    }

    @Override
    public void onRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        onCallBack(gatt, characteristic);
    }

    @Override
    public void onResume() {
        super.onResume();
        trackerAdapter.getNewStepCount();
    }

    @Override
    public void onStart() {
        super.onStart();
        trackerAdapter.getNewStepCount();
        redrawGraph("1");
        //   trackerAdapter.getLastHeartRate();
    }

    public void redrawGraph(String id) {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
        //chart.fitScreen();
        Cursor c = db.rawQuery("select id,result from probes_log where probe_id = " + id + " order by time desc limit 10", null);
        for (int i = 1; i < c.getCount(); i++) {

            if (c.moveToNext()) {

                int resId = c.getColumnIndex("result");
                double res = c.getDouble(resId);
                int  res2= (int) res;
                entries.add(new BarEntry(i, (float)res));
                Log.d(TAG, "redrawGraph: res "+res);
            }
        }
        c = db.rawQuery("select name from probes where id = " + id, null);
        String name = "";
        if (c.moveToNext()) {
            int resName = c.getColumnIndex("name");
            name = c.getString(resName);
        }

        name=getResources().getString(getResources().getIdentifier("probe_name_" + name.toLowerCase(), "string", this.getActivity().getPackageName()));
        Log.d(TAG, "redrawGraph: name "+name);
        BarDataSet dataSet = new BarDataSet(entries, name);
        dataSet.setColor(getResources().getColor(R.color.colorTurquoise));
        final BarData barData = new BarData(dataSet);
        handler.post(new Runnable() {
            @Override
            public void run() {
                chart.setData(barData);
                chart.invalidate();
               // chart.fitScreen();
            //    chart.moveViewToX(10);
            }
        });

    }

    @Override
    public void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        onCallBack(gatt, characteristic);
    }

    public boolean enableStepsNotify() {
        return trackerAdapter.setNotificationSteps(true);
        //   trackerAdapter.getNewStepCount();
    }

    public void getNewHeartrate() {
        trackerAdapter.getNewHeartRate();
        //   dbHelper.clearDb(db);
    }

    public void readProbeResults() {
        data.clear();
        Cursor c = db.query(DbContract.TABLE_NAME_PROBES_LOG, null, null, null, null, null, "id DESC limit 10");

        Cursor Scheme = db.query(DbContract.TABLE_NAME_PROBES_LOG, null, null, null, null, null, "id DESC limit 20");
        if (Scheme != null) {
            if (Scheme.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : Scheme.getColumnNames()) {
                        str = str.concat(cn + " = "
                                + Scheme.getString(Scheme.getColumnIndex(cn)) + "; ");
                    }
                    Log.d(TAG, str);

                } while (Scheme.moveToNext());
            }
            Scheme.close();
        } else
            Log.d(TAG, "Cursor is null");


        Cursor c2 = db.query(DbContract.TABLE_NAME_PROBES, null, null, null, null, null, "id DESC limit 10");
        int[] ids = new int[c2.getCount()];
        String[] names = new String[c2.getCount()];
        int j = c2.getCount() - 1;
        while (c2.moveToNext()) {
            int idColIndex = c2.getColumnIndex("id");
            int nameColIndex = c2.getColumnIndex("name");
            int dynamicsColIndex = c2.getColumnIndex("dynamics");
            ids[j] = c2.getInt(idColIndex);
            names[j] = c2.getString(nameColIndex);
            j--;
            Log.d(TAG, "readProbeResults: " + c2.getInt(idColIndex) + " " + c2.getString(nameColIndex));
        }

        Map<String, Object> m;
        for (int i = 0; i < 50; i++) {
            if (c.moveToNext()) {
                // определяем номера столбцов по имени в выборке
                int idColIndex = c.getColumnIndex("probe_id");
                int resColIndex = c.getColumnIndex("result_text");
                int timeColIndex = c.getColumnIndex("time");
                m = new HashMap<String, Object>();

                /*String olddate = c.getString(timeColIndex);
                android.text.format.DateFormat df = new android.text.format.DateFormat();
                df.format("dd-MM mm:ss", DateFormat.);*/

                Log.d(TAG, "readProbeResults: " + names[c.getInt(idColIndex) - 1] + " " + c.getString(resColIndex) + " " + c.getString(timeColIndex));
                m.put(ATTRIBUTE_NAME_PROBENAME, names[c.getInt(idColIndex) - 1]);
                m.put(ATTRIBUTE_NAME_PROBEDATE, c.getString(timeColIndex));
                m.put(ATTRIBUTE_NAME_PROBERESULT, c.getString(resColIndex));
                m.put(ATTRIBUTE_NAME_ID, c.getInt(idColIndex));
                data.add(m);
                // получаем значения по номерам столбцов и пишем все в лог
                /*Log.d(TAG,
                        "ID = " + c.getInt(idColIndex) +
                                ", value = " + c.getString(nameColIndex) +
                                ", time = " + c.getString(timeColIndex));*/
            } else {
                break;
            }
        }
    }


    public void onCallBack(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        final UUID alertUUID = characteristic.getUuid();

        if (alertUUID.equals(MiBandServices.UUID_HEARTRATE_MEASUREMENT)) {
            final int heartrate = trackerAdapter.getHeartRateFromByteValue(characteristic.getValue());
            counter++;
           /* if (counter == 5) {
                hrActive = false;
                counter = 0;
            }*/
            Log.d(TAG, " * Got heartrate: " + heartrate + " times " + counter);
            //  writeHeartRateToDb(heartrate);

            handler.post(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    pulseCountView.setText(" " + heartrate);
                }
            });
        } else if (alertUUID.equals(MiBandServices.UUID_TOUCH_BUTTON)) {
            Log.d(TAG, " * Got button touch");

        } else if (alertUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
            final int steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
            Log.d(TAG, " * Got steps: " + steps);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    readProbeResults();
                    stepsCountView.setText(" " + steps);
                    simpleAdapter.notifyDataSetChanged();
                }
            });
            mainActivity.writeStepsToDb(steps);
        }
    }

    class MyViewBinder implements SimpleAdapter.ViewBinder {

        int red = getResources().getColor(R.color.colorRed);
        int orange = getResources().getColor(R.color.colorOrange);
        int middle = getResources().getColor(R.color.colorTurquoiseRed);
        int darkGreen = getResources().getColor(R.color.colorTurquoiseDark);
        int green = getResources().getColor(R.color.colorTurquoise);

        @Override
        public boolean setViewValue(View view, Object data,
                                    String textRepresentation) {
            String i;
            switch (view.getId()) {
                // LinearLayout
                case R.id.probe_result:
                    i = ((String) data).toString();

                    if (i.equals(getString(R.string.mark_perfect)))
                        ((TextView) view).setTextColor(green);
                    else if (i.equals(getString(R.string.mark_good)))
                        ((TextView) view).setTextColor(darkGreen);
                    else if (i.equals(getString(R.string.mark_satisfactorily)))
                        ((TextView) view).setTextColor(middle);
                    else if (i.equals(getString(R.string.mark_bad)))
                        ((TextView) view).setTextColor(orange);
                    else if (i.equals(getString(R.string.mark_very_bad)))
                        ((TextView) view).setTextColor(red);

                    ((TextView) view).setText(i);
                   /* <string name="mark_excellent">Excellent</string>
    <string name="mark_perfect">Perfect</string>
    <string name="mark_good">Good</string>
    <string name="mark_satisfactorily">Satisfactorily</string>
    <string name="mark_bad">Bad</string>
    <string name="mark_very_bad">Very bad</string>*/
                    return true;
            }
            return false;
        }
    }
}

