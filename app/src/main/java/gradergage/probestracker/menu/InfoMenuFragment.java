package gradergage.probestracker.menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gradergage.probestracker.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoMenuFragment extends android.support.v4.app.Fragment {

    private View view;
    private String currentProbe;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(this.getResources().getIdentifier("layout_info_" + currentProbe.toLowerCase(), "layout", this.getActivity().getPackageName()), container, false);
        return view;
    }

    public void setText(String name) {
       currentProbe=name;
    }
}
