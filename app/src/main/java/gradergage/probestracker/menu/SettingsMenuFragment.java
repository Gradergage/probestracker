package gradergage.probestracker.menu;

import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import gradergage.probestracker.activities.MainActivity;
import gradergage.probestracker.R;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Thread.sleep;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * create an instance of this fragment.
 */
public class SettingsMenuFragment extends MenuFragment {

    private static final String TAG = "SettingsMenuFragment";
    private TextView textDeviceName;
    private TextView textDeviceMAC;
    private MainActivity context;
    private Button buttonReconnect;
    private Button buttonSave;
    private Button buttonEdit;
    private SharedPreferences pref;

    private RadioButton male;
    private RadioButton female;
    private RadioGroup sex;
    private ArrayList<String> devices;
    private EditText weight;
    private EditText height;
    private EditText age;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onViewStateRestored(savedInstanceState);
        //   Log.d(TAG, "onCreate");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_settings, container, false);
        textDeviceName = (TextView) view.findViewById(R.id.textDeviceName);
        textDeviceMAC = (TextView) view.findViewById(R.id.textViewMAC);

        weight = (EditText) view.findViewById(R.id.editWeight);
        height = (EditText) view.findViewById(R.id.editHeight);
        age = (EditText) view.findViewById(R.id.editAge);
        male = (RadioButton) view.findViewById(R.id.radio_male);
        female = (RadioButton) view.findViewById(R.id.radio_female);
        sex = (RadioGroup) view.findViewById(R.id.radio_sex);
        sex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radio_male:
                        Log.d(TAG, "male");
                        female.setChecked(false);
                        break;
                    case R.id.radio_female:
                        Log.d(TAG, "female");
                        male.setChecked(false);
                        break;
                    default:
                        break;
                }
            }
        });
        devices = adapterBLE.getDevicesList(BluetoothAdapter.getDefaultAdapter());
        String[] items = new String[devices.size()];
        devices.toArray(items);
        Spinner dropdown = (Spinner) view.findViewById(R.id.spinner_devices);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                handler.post(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //rr LinearLayout control=(LinearLayout)view.findViewById(R.id.layout_settings_control);
        //   control.addView(dropdown);
        enableSettings(false);
        // context = (MainActivity)inflater.getContext();
        buttonReconnect = (Button) view.findViewById(R.id.buttonReconnect);
        buttonReconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
            }
        });

        buttonSave = (Button) view.findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSettings();
                ((MainActivity) getActivity()).updateUserInfo();
                enableSettings(false);
            }
        });

        buttonEdit = (Button) view.findViewById(R.id.buttonEdit);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableSettings(true);
            }
        });
        pref = ((MainActivity) getActivity()).getPreferences(MODE_PRIVATE);
        loadSettings();
        //   Log.d(TAG, "onCreateView");
        return view;
    }

    /*  View.OnClickListener radioButtonClickListener = new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              RadioButton rb = (RadioButton)v;
              switch (rb.getId()) {
                  case R.id.radio_male: mInfoTextView.setBackgroundColor(Color.parseColor("#ff0000"));
                      break;
                  case R.id.radio_female: mInfoTextView.setBackgroundColor(Color.parseColor("#0000ff"));
                      break;

                  default:
                      break;
              }
          }
      };*/
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public void setButtonDisconnectDisabled() {
        if (isAdded())
            handler.post(new Runnable() {
                @Override
                public void run() {
                    buttonReconnect.setEnabled(!trackerAdapter.isConnectedToDevice());
                }
            });
    }

    @Override
    public void onConnect() {
        setInfo();
        setButtonDisconnectDisabled();

        SharedPreferences.Editor ed = pref.edit();
        adapterBLE.getPairedDevice().getName();
        ed.putString("settings_device_mac", adapterBLE.getPairedDevice().getName());
        ed.putString("settings_device_name", adapterBLE.getPairedDevice().getAddress());
    }

    public void enableSettings(boolean param) {
        weight.setEnabled(param);
        height.setEnabled(param);
        age.setEnabled(param);
        male.setEnabled(param);
        female.setEnabled(param);
    }

    void saveSettings() {
        SharedPreferences.Editor ed = pref.edit();
        Log.d(TAG, "saveSettings: "+weight.getText().toString()+" "+height.getText().toString()+" "+age.toString());
        if (weight.getText().toString().equals("")||height.getText().toString().equals("")||age.getText().toString().equals(""))
        {

            Toast toast =Toast.makeText(getContext(),getString(R.string.string_error_wrong_settings),Toast.LENGTH_SHORT);
            toast.show();
            loadSettings();
            return;
        }

            ed.putString("settings_weight", weight.getText().toString());

            ed.putString("settings_height", height.getText().toString());

            ed.putString("settings_age", age.getText().toString());

            ed.putString("settings_device_mac", age.getText().toString());
            ed.putString("settings_device_name", age.getText().toString());



        if (male.isChecked()) {
            ed.putBoolean("settings_sex", true);
        } else {
            ed.putBoolean("settings_sex", false);
        }
        //     ed.putBoolean("settings_sex",)
        ed.commit();
        ((MainActivity) getActivity()).updateUserInfo();
    }

    @Override
    public void onStart() {
        super.onStart();
        setButtonDisconnectDisabled();
        if (trackerAdapter.isConnectedToDevice()) {
            setInfo();
        }
    }

    void loadSettings() {
        pref = ((MainActivity) getActivity()).getPreferences(MODE_PRIVATE);
        String w = pref.getString("settings_weight", "");
        String h = pref.getString("settings_height", "");
        String a = pref.getString("settings_age", "");
        String deviceName = pref.getString("settings_device_mac", "");
        String deviceMac = pref.getString("settings_device_name", "");
        boolean s = pref.getBoolean("settings_sex", true);
        weight.setText(w);
        height.setText(h);
        age.setText(a);
        male.setChecked(s);
        female.setChecked(!s);
        Log.d(TAG, " ** Settings read" + w + " " + h + " " + a + " " + s);
    }

    public void setInfo() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (trackerAdapter.isConnectedToDevice()) {

                    if (isAdded()) {
                        textDeviceName.setText(adapterBLE.getPairedDevice().getName());
                        textDeviceMAC.setText(adapterBLE.getPairedDevice().getAddress());
                        buttonReconnect.setText(getString(R.string.action_connected));
                    }

                    //  Log.d(TAG, "Connected");
                } else {
                    try {
                        textDeviceName.setText("No device");
                        textDeviceMAC.setText(" ");
                        //     Log.d(TAG, "Disconnected");
                        buttonReconnect.setText(getString(R.string.action_connect));
                    } catch (java.lang.IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void connect() {
        if (!trackerAdapter.connectToDevice()) {
            Toast toast = Toast.makeText(getContext(), "Connection failed", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
