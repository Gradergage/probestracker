package gradergage.probestracker.menu;

import android.app.Fragment;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import device.AdapterBLE;
import device.OnBluetoothInteractionListener;
import device.TrackerAdapter;
import gradergage.probestracker.R;
import gradergage.probestracker.activities.MainActivity;
import probes.UserParameters;
import tools.TimerListener;

/**
 * Created by Gradergage on 07.06.2018.
 */

public abstract class MenuFragment extends android.support.v4.app.Fragment implements TimerListener, OnBluetoothInteractionListener {

    public Handler handler;
    public AdapterBLE adapterBLE;
    public TrackerAdapter trackerAdapter;
    public SQLiteDatabase db;
    public UserParameters user;
    public View view;
    public MainActivity mainActivity;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (handler == null)
            handler = ((MainActivity) getActivity()).getHandler();

        if (adapterBLE == null)
            adapterBLE = ((MainActivity) getActivity()).getAdapterBLE();

        if (trackerAdapter == null)
            trackerAdapter = ((MainActivity) getActivity()).getTrackerAdapter();

        if (db == null)
            db = ((MainActivity) getActivity()).getDB();

        if (user == null)
            user = ((MainActivity) getActivity()).getUser();

        mainActivity = ((MainActivity) getActivity());
    }

    @Override
    public void onDisconnect() {
    }

    @Override
    public void onConnect() {
    }

    @Override
    public void onDiscovered() {
    }

    @Override
    public void onRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
    }

    @Override
    public void onWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
    }

    @Override
    public void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
    }

    @Override
    public void onSecond() {
    }

    @Override
    public void onMinute() {
    }

    @Override
    public void onFinish() {
    }
}
