package gradergage.probestracker.menu;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import DBtools.DbContract;
import gradergage.probestracker.activities.MainActivity;
import gradergage.probestracker.activities.ProbeActivity;
import gradergage.probestracker.R;
import probes.UserParameters;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class ProbesMenuFragment extends MenuFragment {
    private final String TAG = "ProbesMenuFragment";
    private View view;
    private MainActivity mainActivity;
    final String ATTRIBUTE_NAME_PROBENAME = "probename";
    private ListView probesList;
    SimpleAdapter simpleAdapter;
    private String[] list;
    ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

    public ProbesMenuFragment() {
        //super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      /*  ListView lvMain = (ListView) findViewById(R.id.lvProbes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, probes);

        lvMain.setAdapter(adapter); */

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        view = inflater.inflate(R.layout.fragment_menu_probes, container, false);
        readProbeList();
        mainActivity = (MainActivity) getActivity();
        probesList = (ListView) view.findViewById(R.id.probes_list_view);

        probesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.d(TAG, "itemSelect: position = " + i + ", id = "
                        + l);
                TextView text = (TextView) view.findViewById(R.id.listview_probes_probe_name);
                startActivity(list[i],i+1);
            }
        });
        String[] from = {ATTRIBUTE_NAME_PROBENAME};
        int[] to = {R.id.listview_probes_probe_name};
        simpleAdapter = new SimpleAdapter(view.getContext(), data, R.layout.listview_probes,
                from, to);
        probesList.setAdapter(simpleAdapter);
        return view;
    }

    public void startActivity(String probeName,int id) {
        String className = getClassNameFromProbe(probeName);
        try {
            Class<?> c = Class.forName(className);
        } catch (ClassNotFoundException e) {
          //  Log.d(TAG, "Class " + className + " does not exist");
            return;
        }
        FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


        TestFrameFragment test = new TestFrameFragment();
        test.setProbeName(probeName,id);
        fragmentTransaction.replace(R.id.pageContent, test);
        fragmentTransaction.commit();
        //  Intent intent = new Intent(mainActivity, ProbeActivity.class);
        //  intent.putExtra("contentName",probeName);
        //  intent.putExtra("main",((MainActivity)getActivity()));
        //   startActivity(intent);
    }

    public String getClassNameFromProbe(String name) {
        Log.d(TAG, "getClassNameFromProbe: gradergage.probestracker.tests." + name + "TestFragment");
        return "gradergage.probestracker.tests." + name + "TestFragment";
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void readProbeList() {
        data.clear();
        Cursor c = db.query(DbContract.TABLE_NAME_PROBES, null, null, null, null, null, null);
        Map<String, Object> m;
        list = new String[c.getCount()];
        int i = 0;

        while (c.moveToNext()) {
            int nameColIndex = c.getColumnIndex("name");
            Log.d(TAG, "readProbeList: "+c.getString(nameColIndex));
            m = new HashMap<String, Object>();
            list[i] = c.getString(nameColIndex);
            i++;
            String name = getString(view.getContext().getResources().getIdentifier("probe_name_" + c.getString(nameColIndex).toLowerCase(), "string", view.getContext().getPackageName()));
            m.put(ATTRIBUTE_NAME_PROBENAME, name);
            data.add(m);
        }
    }

    public UserParameters getUser() {
        return ((ProbeActivity) getActivity()).getUser();
    }

}
