package gradergage.probestracker.activities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import gradergage.probestracker.R;
import probes.Probe;
import probes.UserParameters;

public class ProbeActivity extends AppCompatActivity {


    private final String TAG = "ProbeActivity";
    private ConstraintLayout probeLayout;
    private Handler handler;
    private UserParameters user;
    private Probe test;
    private SharedPreferences pref;
    private Button btnStart;
    private Button btnInfo;
    private TextView probeName;
    private TextView probeResult;
    private String currentProbe;
    private ArrayList<Map<String, Object>> probesList;
    private MainActivity mainActivity;

    /*public ProbeActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_test_frame);
        probeName = (TextView) findViewById(R.id.text_probe_name);
        probeResult = (TextView) findViewById(R.id.text_probe_result);
        probeLayout = (ConstraintLayout) findViewById(R.id.layout_probe_content);

        btnStart = (Button) findViewById(R.id.btn_probe_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start();
            }
        });
        handler = new Handler(Looper.getMainLooper());
        btnInfo = (Button) findViewById(R.id.btn_probe_info);
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                info();
            }
        });
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        user = new UserParameters(pref);
        Intent intent = getIntent();
        currentProbe = intent.getStringExtra("contentName");
        if(getBaseContext()==null)
        {
            Log.d(TAG, "parent==null");
        }
       // mainActivity=(MainActivity)intent.getSerializableExtra("name");
        setContent(currentProbe);
    }

    public void setContent(String name) {
        Object probeInstance = null;

        try {
            Class<?> c = Class.forName(getClassNameFromProbe(name));
            Log.d(TAG, "Class found = " + c.getName());
            Log.d(TAG, "Package = " + c.getPackage());
            Constructor<?> ctor = c.getConstructor();
            probeInstance = ctor.newInstance();
            test = (Probe) probeInstance;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if (probeInstance != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.layout_probe_content, (android.support.v4.app.Fragment) probeInstance);
            fragmentTransaction.commit();
            //this.getResources().getIdentifier("nameOfDrawable", "string", this.getPackageName());
            probeName.setText(getString(this.getResources().getIdentifier("probe_name_" + name, "string", this.getPackageName())));

        } else {
            Log.d(TAG, "setContent:" + false);
        }
    }

    public void loadProbesFragments() {
        HashMap temp = new HashMap<String, Object>();
    }

    public String getClassNameFromProbe(String name) {
        return "gradergage.probestracker.tests." + name + "TestFragment";
    }

    public void showInfo() {

    }

    public void start() {
        test.start();
    }

    public void info() {
        test.start();
    }

    public void showResult() {
    }

    public Handler getHandler() {
        return handler;
    }

    public double convertStepsToDistance(int steps) {
        double distance = 1.0 * user.getHeight() / 4 + 0.37;
        return distance;
    }


    public UserParameters getUser() {
        return user;
    }

    public void updateUserInfo() {
        user = new UserParameters(pref);
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }
}
