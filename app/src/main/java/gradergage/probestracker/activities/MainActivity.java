package gradergage.probestracker.activities;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import java.io.Serializable;
import java.util.UUID;

import device.*;
import DBtools.*;
import gradergage.probestracker.R;
import gradergage.probestracker.menu.HomeMenuFragment;
import gradergage.probestracker.menu.ProbesMenuFragment;
import gradergage.probestracker.menu.SettingsMenuFragment;
import probes.UserParameters;

public class MainActivity extends AppCompatActivity implements OnBluetoothInteractionListener {

    public static final String TAG = "MainActivity";
    private TextView mTextMessage;

    private ProbesMenuFragment probesMenuFragment;
    private SettingsMenuFragment settingsMenuFragment;
    private HomeMenuFragment homeMenuFragment;
    private UserParameters user;
    private SharedPreferences pref;
    private Handler handler;

    private AdapterBLE adapterBLE;
    private TrackerAdapter trackerAdapter;
    private SQLiteDatabase db;
    private BottomNavigationView navigation;
    private int stepsCorrection;
    private boolean isConnectedToDevice = false;

    public BottomNavigationView getNavigation() {
        return navigation;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_settings:
                    if (settingsMenuFragment == null) {
                        Log.d(TAG, " settingsMenuFragment == null");
                        return true;
                    }
                    fragmentTransaction.replace(R.id.pageContent, settingsMenuFragment);
                    fragmentTransaction.commit();
                    return true;

                case R.id.navigation_probes:

                    if (probesMenuFragment == null) {
                        Log.d(TAG, " probesMenuFragment == null");
                        return true;
                    }
                    fragmentTransaction.replace(R.id.pageContent, probesMenuFragment);
                    fragmentTransaction.commit();
                    return true;

                case R.id.navigation_main:

                    if (homeMenuFragment == null) {
                        Log.d(TAG, " homeMenuFragment == null");
                        return true;
                    }
                    fragmentTransaction.replace(R.id.pageContent, homeMenuFragment);
                    fragmentTransaction.commit();

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        probesMenuFragment = new ProbesMenuFragment();
        settingsMenuFragment = new SettingsMenuFragment();
        homeMenuFragment = new HomeMenuFragment();
        mTextMessage = (TextView) findViewById(R.id.message);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_main);

        DbHelper dbHelper = new DbHelper(this);
        db = dbHelper.getWritableDatabase();
        handler = new Handler(Looper.getMainLooper());
        adapterBLE = new AdapterBLE(this, handler);
        trackerAdapter = new AdapterMiBand2(adapterBLE);

        adapterBLE.addListener(this);
        adapterBLE.addListener(settingsMenuFragment);
        adapterBLE.addListener(homeMenuFragment);


        pref = getPreferences(MODE_PRIVATE);
        user = new UserParameters(pref);
        if (!pref.contains("settings_first_launch")) {
            SharedPreferences.Editor ed = pref.edit();
            ed.putBoolean("settings_first_launch", false);

         //   dbHelper.loadDbFirstTime(db);
            /*for(int i=0;i<10;i++)
          dbHelper.addFakeData(db);*/
        //    dbHelper.drop(db);
       // dbHelper.loadDbFirstTime(db);dbHelper.addFakeData(db);
        }

        if(!trackerAdapter.connectToDevice())
        { Toast toast =Toast.makeText(getApplicationContext(),getString(R.string.string_error_connection),Toast.LENGTH_SHORT);
        toast.show();
        }
        // cv.put("dynamics","0");

    }

    @Override
    public void onStart() {
        super.onStart();
       /*if (!isConnectedToDevice) {
            trackerAdapter.connectToDevice();
        }*/

    }

    @Override
    public void onDisconnect() {
        isConnectedToDevice = false;
    }

    /*  public void enableStepsNotify(View v) {
          trackerAdapter.setNotificationSteps();
      }

      public void getNewHeartrate(View v) {
          trackerAdapter.getNewHeartRate();
          //   dbHelper.clearDb(db);
      }
  */
    public void testLog() {
        Log.d(TAG, " *** test");
    }

    @Override
    public void onConnect() {
        trackerAdapter.setNotificationButton();
        //  trackerAdapter.setNotificationSteps();
    }

    @Override
    public void onDiscovered() {
        Log.d(TAG, " *** Setting HR notification");
        trackerAdapter.setNotificationHeartRate();
        Log.d(TAG, " *** HR set");
        trackerAdapter.getNewStepCount();
        //trackerAdapter.setNotificationSteps();
    }

    public AdapterBLE getAdapterBLE() {
        return adapterBLE;
    }

    public Handler getHandler() {
        return handler;
    }

    public TrackerAdapter getTrackerAdapter() {
        return trackerAdapter;
    }

    @Override
    public void onRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        final UUID readUUID = characteristic.getUuid();
        if (readUUID.equals(MiBandServices.UUID_HEARTRATE_MEASUREMENT)) {
            final int heartrate = trackerAdapter.getHeartRateFromByteValue(characteristic.getValue());
            Log.d(TAG, " * Read heartrate: " + heartrate);
        } else if (readUUID.equals(MiBandServices.UUID_TOUCH_BUTTON)) {
            Log.d(TAG, " * Read button touch");
        } else if (readUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
            final int steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
            stepsCorrection = steps;
            Log.d(TAG, " *Read steps: " + steps);
            writeStepsToDb(steps);
        }
    }

    public void writeHeartRateToDb(int hr) {
        if (hr == 0) {
            return;
        }
        db.execSQL("INSERT INTO " + DbContract.TABLE_NAME_HEARTRATE + "(value,time) VALUES (" + hr + ", datetime('now')) ");
    }

    public SQLiteDatabase getDB() {
        return db;
    }

    public void writeStepsToDb(int steps) {
        if (steps == 0) {
            return;
        }

        db.execSQL("INSERT INTO " + DbContract.TABLE_NAME_STEPS + "(value,time) VALUES (" + steps + ", datetime('now')) ");
        Cursor c = db.query(DbContract.TABLE_NAME_STEPS, null, null, null, null, null, null);

        if (c.moveToLast()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("value");
            int timeColIndex = c.getColumnIndex("time");
            // получаем значения по номерам столбцов и пишем все в лог
           /* Log.d(TAG,
                    "ID = " + c.getInt(idColIndex) +
                            ", value = " + c.getString(nameColIndex) +
                            ", time = " + c.getString(timeColIndex));*/
        } else
            Log.d(TAG, "0 rows");
    }

    public void writeProbesToDb(int steps) {
        ContentValues cv = new ContentValues();
        long rowID = db.insert(DbContract.TABLE_NAME_STEPS, null, cv);
        Cursor c = db.query("mytable", null, null, null, null, null, null);
    }

    @Override
    public void onWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

    }

    @Override
    public void onNotification(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        //onCallBack(gatt, characteristic);
        final UUID alertUUID = characteristic.getUuid();

        if (alertUUID.equals(MiBandServices.UUID_HEARTRATE_MEASUREMENT)) {
            final int heartrate = trackerAdapter.getHeartRateFromByteValue(characteristic.getValue());
            Log.d(TAG, " * Got heartrate: " + heartrate);
            writeHeartRateToDb(heartrate);

        } else if (alertUUID.equals(MiBandServices.UUID_TOUCH_BUTTON)) {
            //  Log.d(TAG, " * Got button touch");

        } else if (alertUUID.equals(MiBandServices.UUID_CHARACTERISTIC_STEPS)) {
            final int steps = trackerAdapter.getStepsFromByteValue(characteristic.getValue());
            stepsCorrection = steps;
            //   Log.d(TAG, " * Got steps: " + steps);
            //     writeStepsToDb(steps);
        }
    }

    public UserParameters getUser() {
        return user;
    }

    public void updateUserInfo() {
        user = new UserParameters(pref);
    }

    public int getStepsCorrection() {
        return stepsCorrection;
    }
}
